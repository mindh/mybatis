# mygenerator
基于mybatis-generator-core-1.3.7，增加了MyCommentGenerator，可以自动生成中文注释，目前适用于oracle和mysql
#### 本项目参考
https://www.jb51.net/article/148070.htm <br>
该项目github原址为：https://github.com/mizhoux/mbg-comment
#### 关于文档注释生效的generatorConfig.xml的相关配置
参考：https://blog.csdn.net/qq_21251983/article/details/52849079
